import java.util.Scanner;
public class Roulette{
    public static void main (String[] args){

        Scanner sc = new Scanner(System.in);
        RouletteWheel rouletteWheel = new RouletteWheel();

       //initial number to be guessed 
        int numToBeGuessed = 11;

       //Spin 
        System.out.println("Spin the roulette wheel" +"\n"+ "..... ..... ....." + "\n");
        rouletteWheel.spin();
        System.out.println("The last spin value is: " + rouletteWheel.getValue());
       
       //GamePlay Loop 
        boolean gameOver = false;
        int wrongGuesses=0;
        while(!gameOver)
        {
           //If the both values are not identical 
            if(rouletteWheel.getValue() != numToBeGuessed)
            {
                wrongGuesses++;
                System.out.println("Wrong Guesses Counter: " +wrongGuesses);
               //End the game if you have at least 6 wrong guesses 
                if(wrongGuesses == 6)
                {
                    System.out.println("Game Over! You have reached 6 wrong Guesses");
                    gameOver = true;
                    break;
                }

                System.out.println("Incorrect Guess! Would you like to Spin Again? Yes (1) or No (2)");
                int nextRound = sc.nextInt();
               //Re spin if user wants to play another round 
                if(nextRound == 1)
                {
                    rouletteWheel.spin();
                    System.out.println("\n" + "The last spin value is: " + rouletteWheel.getValue());
                }
               //otherWise the game is over
                else
                {
                    System.out.println("\n"+ "Game Over! You Lost");
                    gameOver = true;
                }
            }
            else
            {
                System.out.println("\n"+ "You won!!!");
                gameOver = true;
            }
        }
    }
}
