import java.util.Random;
public class RouletteWheel{

    private Random randomGen;
    private int lastSpinNum;

   //Constructor 
    public RouletteWheel()
	{
		this.randomGen = new Random();
		this.lastSpinNum = 0;
	}

   //Generate random number from 0-37 
    public void spin(){
        this.lastSpinNum = randomGen.nextInt(37);
    }
    
   //Return the spin value 
    public int getValue(){
        return this.lastSpinNum;
    }
}
